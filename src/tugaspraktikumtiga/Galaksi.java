package tugaspraktikumtiga;

public class Galaksi {
    protected String namaGalaksi;
    protected int luas;
    protected String tipe;
    protected int massa;
    protected int jumlahBintang;

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public int getMassa() {
        return massa;
    }

    public void setMassa(int massa) {
        this.massa = massa;
    }

    public int getJumlahBintang() {
        return jumlahBintang;
    }

    public void setJumlahBintang(int jumlahBintang) {
        this.jumlahBintang = jumlahBintang;
    }

    public String getNamaGalaksi() {
        return namaGalaksi;
    }

    public void setNamaGalaksi(String namaGalaksi) {
        this.namaGalaksi = namaGalaksi;
    }

    public int getLuas() {
        return luas;
    }

    public void setLuas(int luas) {
        this.luas = luas;
    }
    
    
    
    
}
