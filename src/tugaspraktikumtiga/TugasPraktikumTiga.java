package tugaspraktikumtiga;
import java.util.Scanner;
import java.util.ArrayList;

public class TugasPraktikumTiga {

    public static void start(){
        ArrayList<Galaksi> galaksiBaru;
        galaksiBaru = new ArrayList<>();
        int loop;
        
        do{
            Scanner input = new Scanner(System.in);
        
            int pilih;
            System.out.println("================================");
            System.out.println("1. Masukkan data");
            System.out.println("2. Tampilkan data");
            System.out.println("3. Update data");
            System.out.println("4. Hapus data");
            System.out.print("Pilihan anda : ");
            pilih = input.nextInt();
        
            switch (pilih) {
                case 1:
                    System.out.println("================================");
                    System.out.print("Masukkan jumlah data yang ingin dimasukkan : ");
                    int jumlah;
                    jumlah = input.nextInt();
                    for(int a=0;a<jumlah;a++){
                    
                        galaksiBaru.add(new Galaksi());
                        System.out.println(" ");
                        System.out.print("Nama galaksi : ");
                        galaksiBaru.get(a).setNamaGalaksi(input.next());
                        System.out.print("Tipe galaksi : ");
                        galaksiBaru.get(a).setTipe(input.next());
                        System.out.print("Luas galaksi : ");
                        galaksiBaru.get(a).setLuas(input.nextInt());
                        System.out.print("Massa galaksi : ");
                        galaksiBaru.get(a).setMassa(input.nextInt());
                        System.out.print("Jumlah Bintang : ");
                        galaksiBaru.get(a).setJumlahBintang(input.nextInt());
                        System.out.println("Data berhasil diinputkan");   
                        
                    }   
                
                case 2:
                    System.out.println("================================");
                    System.out.println("Menampilkan data : ");
                    for (int a=0;a<galaksiBaru.size();a++) {
                        System.out.println("Nama galaksi : " + galaksiBaru.get(a).getNamaGalaksi());
                        System.out.println("Tipe galaksi : " + galaksiBaru.get(a).getTipe());
                        System.out.println("Luas galaksi : " + galaksiBaru.get(a).getLuas());
                        System.out.println("Massa galaksi : " + galaksiBaru.get(a).getMassa());
                        System.out.println("Jumlah Bintang : " + galaksiBaru.get(a).getJumlahBintang());
                        System.out.println(" ");
                    }   
                    break;
                case 3:
                    int update;
                    System.out.println("================================");
                    System.out.print("Masukkan Indeks Galaksi yang ingin diupdate : ");
                    update = input.nextInt() - 1;
                    System.out.print("Nama galaksi : ");
                    galaksiBaru.get(update).setNamaGalaksi(input.next());
                    System.out.print("Tipe galaksi : ");
                    galaksiBaru.get(update).setTipe(input.next());
                    System.out.print("Luas galaksi : ");
                    galaksiBaru.get(update).setLuas(input.nextInt());
                    System.out.print("Massa galaksi : ");
                    galaksiBaru.get(update).setMassa(input.nextInt());
                    System.out.print("Jumlah Bintang : ");
                    galaksiBaru.get(update).setJumlahBintang(input.nextInt());
                    System.out.println("Galaksi berhasil diupdate");
                    break;
                case 4:
                    int del;
                    System.out.println("================================");
                    System.out.print("Masukkan Indeks Galaksi yang ingin dihapus : ");
                    del = input.nextInt() - 1;
                    galaksiBaru.remove(del);
                    System.out.println("Galaksi berhasil dihapus");
                    break;                
                default:
                    System.out.println("Input anda salah.");
            }
            System.out.print("1 untuk kembali ke menu, 0 untuk keluar : ");
            loop = input.nextInt();
        }
        while (loop == 1);
    }
    
    public static void main(String[] args) {
      start();  
    }
    
}

